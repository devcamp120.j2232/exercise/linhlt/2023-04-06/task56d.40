package com.devcamp.arrayfilterinputapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayfilterinputapi.services.IntegerFilter;

@RestController
@RequestMapping("/")
@CrossOrigin
public class IntegerFilterController {
    @Autowired
    IntegerFilter integerFilter;
    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> integerFilterApi(@RequestParam (value="pos", defaultValue = "0" ) int pos){
        return integerFilter.searchIntegers(pos);
    }
    @GetMapping("/array-int-param/{index}")
    public int getIntegerApi(@PathVariable int index){
        return integerFilter.getInteger(index);
    }
}
