package com.devcamp.arrayfilterinputapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
@Service
public class IntegerFilter {
    private int[] rainbows = {1, 23, 32, 43, 54, 65, 86, 10,15,16,18};
    public ArrayList<Integer> searchIntegers(int pos){
        ArrayList<Integer> integerFilter = new ArrayList<>();
        for (int integer: rainbows){
            if (integer > pos){
                integerFilter.add(integer);
            }
        }
        return integerFilter;
    }
    public int getInteger(int index){
        int integer = -1;
        if (index >=0 && index <=10){
            integer = rainbows[index];
        }
        return integer;
    }
}
